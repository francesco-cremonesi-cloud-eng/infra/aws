terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
	  profile = "default"
	  region = "eu-west-3"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
	  version = ">=2.9.0"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
  }
  required_version = ">= 0.13"
}
