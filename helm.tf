provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.my-cluster.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.my-cluster.certificate_authority.0.data)
	token                  = data.aws_eks_cluster_auth.my-auth.token
	load_config_file       = false
  }
}

resource "helm_release" "gitlab_helm" {
  name  = "gitlab"
  chart = "gitlab"
  repository = "https://charts.gitlab.io/"
  
  set {
    name  = "certmanager.install"
    value = "false"
  }
  
  set {
    name  = "global.ingress.configureCertmanager"
    value = "false"
  }  
  
  set {
    name  = "gitlab-runner.install"
    value = "false"
  }   
  
  set {
    name  = "global.hosts.domain"
    value = "example.com"
  } 
    
  set {
    name  = "global.hosts.https"
    value = "false"
  }
  
 
  set {
    name  = "nginx-ingress.controller.service.annotations.service.beta.kubernetes.io/aws-load-balancer-internal"
    value = "true"
  } 
    
  set {
    name  = "nginx-ingress.controller.service.annotations.service.beta.kubernetes.io/aws-load-balancer-backend-protocol"
    value = "tcp"
  }  
  
}

