resource "aws_route53_zone" "private" {
  name = "example.com"

  vpc {
    vpc_id = module.vpc.vpc_id
  }
}

#resource "aws_route53_record" "gitlab-example-com" {
#  zone_id = aws_route53_zone.private.zone_id
#  name    = "gitlab.example.com"
#  type    = "A"
#  ttl     = "300"
#  records = [aws_eip.lb.public_ip]
#}