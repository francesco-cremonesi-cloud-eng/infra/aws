module "eks" {
  source           = "terraform-aws-modules/eks/aws"
  cluster_name     = "cremonesi-eks"
  cluster_version  = "1.17"
  subnets          = module.vpc.public_subnets
  write_kubeconfig = "false"
  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
  vpc_id = module.vpc.vpc_id

  worker_groups = [
    {
      instance_type = "t2.xlarge"
      asg_max_size  = 30
      tags = [{
        key                 = "Terraform"
        value               = "true"
        propagate_at_launch = true
      }]
    }
  ]
}

output "env-dynamic-url" {
  value = module.eks.cluster_endpoint
}

output "kubectl-config-map" {
  value = module.eks.config_map_aws_auth
}

output "kubeconfig" {
  value = module.eks.kubeconfig
}

output "auth-data" {
  value = module.eks.cluster_certificate_authority_data
}